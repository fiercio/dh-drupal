<?php
namespace dh\autoinstalator;

class DostepDoPanelu extends CustomizerDataElement
{
    /**
     * @var string Nazwa elemenu strony do którego zdefiniowane są dane logowania (np. PANEL ADMINISTRACYJNY)
     */
    public $nazwa;

    /**
     * @var string URL (wraz z protokołem na początku)
     */
    public $url;

    /**
     * @var string Nazwa użytkownika
     */
    public $login;

    /**
     * @var string Hasło
     */
    public $haslo;


    /**
     * Konstruktor pozwalajacy w prosty sposob utworzyc obiekt
     *
     * @param string $nazwa
     * @param string $url
     * @param string $login
     * @param string $haslo
     */
    public function __construct($nazwa, $url, $login, $haslo)
    {
        parent::__construct();
        $this->nazwa = $nazwa;
        $this->url = $url;
        $this->login = $login;
        $this->haslo = $haslo;
    }
}