<?php
namespace dh\autoinstalator;

class CustomizerDataElement
{
    /**
     * @var string Nazwa klasy
    */
    public $_className;

    public function __construct()
    {
        $this->_className = get_class($this);
    }
}