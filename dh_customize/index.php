<?php
/** Dolaczenie wymaganych klas **/
require_once 'class.CustomizerDataElement.php';
require_once 'class.DostepDoPanelu.php';

/** Pobranie argumentów **/
$input = json_decode( file_get_contents('php://input') );

/** Połączenie z kliencką bazą danych **/
try {
	$db = new \PDO('mysql:host=' . $input->env->BazaMysql->host . ';dbname=' . $input->env->BazaMysql->nazwa, $input->env->BazaMysql->uzytkownik, $input->env->BazaMysql->haslo);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$db->exec('SET NAMES utf8');
} catch (\Exception $e) {
	_fail('Blad polaczenia z baza danych - ' . $e->getMessage());
}

/** Wczytanie dumpu SQL do bazy danych **/
$dumpFilename = 'dump.sql';
$dumpTempLine = '';
$dumpLines = file($dumpFilename);

foreach($dumpLines as $dumpLine)
{
	if(substr($dumpLine, 0, 2) == '--' OR $dumpLine == '')
		continue;

	$dumpTempLine .= $dumpLine;
	if (substr(trim($dumpTempLine), -1, 1) == ';')
	{
		try {
			$db->exec($dumpTempLine);
			$dumpTempLine = '';
		} catch (\Exception $e) {
			_fail('Blad w imporcie dumpu sql - ' . $e->getMessage() . ' - w zapytaniu ' . $dumpTempLine);
		}
	}
}

/** Wygenerowanie danych użytkownika administracyjnego **/
$adminMail = $input->env->KontoEmail->uzytkownik;
$adminLogin = 'admin';
$adminPass = substr(md5(sha1(date(DATE_ATOM).'dh123')), 0, 8);


/** Aktualizacja dodatkowych parametrow aplikacji w bazie danych **/
try {

	define('DRUPAL_ROOT', dirname(getcwd()));

	include_once DRUPAL_ROOT . '/includes/password.inc';
	include_once DRUPAL_ROOT . '/includes/bootstrap.inc';

	$newhash = user_hash_password($adminPass);

	$stmt = $db->prepare('UPDATE users SET mail=:userEmail, pass=:userPass WHERE uid=1');
	$stmt->bindValue(':userEmail', $adminMail);
	$stmt->bindValue(':userPass', $newhash);
	$stmt->execute();

} catch (\Exception $e) {
	_fail('Blad w aktualizacji dodatkowych parametrow aplikacji - ' . $e->getMessage() );
}

/** Aktualizacja pliku konfiguracyjnego **/
$configTpl = file_get_contents('config.tpl');
$configReplaceArray = array(
	'DB_NAME' => $input->env->BazaMysql->nazwa,
	'DB_USER' => $input->env->BazaMysql->uzytkownik,
	'DB_PASS' => $input->env->BazaMysql->haslo,
	'DB_HOST' => $input->env->BazaMysql->host,
);
$configTpl = replacer($configTpl, $configReplaceArray);
file_put_contents('../sites/default/settings.php', $configTpl);


/** Usuniecie zaslepki "Strona w budowie" **/
unlink('../index.html');


/** Usuniecie katalogu dh_customize **/
rrmdir('../dh_customize/');


/** Zwrocenie danych o aplikacji **/
$x = array(
    new \dh\autoinstalator\DostepDoPanelu(
        'Panel administracyjny',
        'http://' . $input->env->HttpVhost->subdomena . '/admin/',
        'admin',
        $adminPass
    )
);

echo json_encode($x);


/**********************************************************************************************************************/

/**
 * Funkcja konczaca awaryjnie wykonywanie skryptu i zglaszajaca blad (kod http 500)
 * @param string $message
 */
function _fail($message)
{
	header('HTTP/1.1 500 Internal Server Error');
	echo 'err:' . $message;
	exit;
}

/**
 * Funkcja usuwajaca rekurencyjnie katalog wraz z cala zawartoscia
 * @param string $dir Nazwa katalogu do usuniecia
 */
function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
}

/**
 * Inteligentne zastepowanie zmiennych
 * @param string $subject Ciag-szablon
 * @param array $replaceArray Tablica elementow, gdzie klucz to token w szablonie, a wartosc - to czym nalezy go zastapic.
 * @return string
 */
function replacer($subject, $replaceArray)
{
	$from = array();
	$to = array();

	foreach($replaceArray as $key=>$val)
	{
		$from[] = '{' . $key . '}';
		$to[] = $val;
	}

	return str_replace($from, $to, $subject);
}
